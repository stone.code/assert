// Package assert provide a single function to emulate C-style asserts.
//
// The goal of this package is to provide a call that will document and enforce
// preconditions, postconditions, and invariants in code.  Failures should
// only result from program bugs.  This package should not be used to replace
// proper error handling.
//
// # Assertions
//
// Each assertion documents that a predicate is true when the assertion is
// executed. Depending on the build, that predicate might also be enforced
// (i.e. by terminating the program, or, more gently, by panicking).
// There are three types of assertion:  (1) preconditions, which document
// conditions (specified as predicates) for calling a function, (2)
// postconditions, which document conditions at the end of a function, and
// (3) invariant, which specify conditions over a particular scope or for
// a type.
//
// If the predicate for an assertion is false, this conditions indicates a bug
// in the code (as opposed to a transient or run-time error).  When enforced,
// assertions are a debugging tool.
//
// # References
//
//   - https://golang.org/doc/faq#assertions
//   - https://golang.org/ref/spec#Run_time_panics
package assert

// Error contains error information from a failed call to Assert.
type Error string

// Error returns a string describing the error.
func (e Error) Error() string {
	return "assertion error: " + string(e)
}

// RuntimeError is a no-op function but serves to distinguish types that are
// run time errors from ordinary errors: a type is a run time error if it has a
// RuntimeError method.
//
// See:  https://godoc.org/runtime#Error
func (e Error) RuntimeError() {}

// Assert will panic if the value of predicate is false.
//
// The panic's value will have type assert.Error.  Note that the filename and
// line number are not recorded, but will be available if a stack trace is
// printed.
func Assert(predicate bool, message string) {
	if !predicate {
		panic(Error(message))
	}
}
