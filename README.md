# Package assert

Package assert provide a single function to emulate C-style asserts.

The goal of this package is to provide a call that will document and enforce
preconditions, post-conditions, and invariants in code.  Failures should
only result from program bugs.  This package should not be used to replace
proper error handling.

References:

* [Why does Go not have assertions?](https://golang.org/doc/faq#assertions)
* [Run-time panics](https://golang.org/ref/spec#Run_time_panics)
* [How to use assertions in C](https://ptolemy.berkeley.edu/~johnr/tutorials/assertions.html)
* [Assert() And Similar Macros In SQLite](https://www.sqlite.org/assert.html)

## Install

The package can be installed from the command line using the [go](https://golang.org/cmd/go/) tool.  There are no dependencies beyond the standard library.  Any version of Go should work, but testing only covers version 1.8 and up.

    go get gitlab.com/stone.code/assert

## Getting Started

Package documentation and examples are on [godoc](https://godoc.org/gitlab.com/stone.code/assert).

## Contribute

Feedback and PRs welcome.  You can also [submit an issue](https://gitlab.com/stone.code/assert/issues).

Despite its small size, this package is considered to be feature complete.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stone.code/assert)](https://goreportcard.com/report/gitlab.com/stone.code/assert)

# Related Projects

* [github.com/negrel/assert](https://pkg.go.dev/github.com/negrel/assert):  Package assert provides a set of comprehensive debug assertions. 

## License

BSD (c) Robert Johnstone