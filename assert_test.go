package assert_test

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
	"testing"

	"gitlab.com/stone.code/assert"
)

var (
	_ error         = assert.Error("")
	_ runtime.Error = assert.Error("")
)

func ExampleAssert() {
	assert.Assert(true, "trivially true precondition")
	fmt.Println("OK")

	// Output:
	// OK
}

func ExampleAssert_fail() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()

	assert.Assert(false, "trivially false precondition")
	fmt.Println("OK")

	// Output:
	// assertion error: trivially false precondition
}

func ExampleAssert_sqrt() {
	// The square root function in package math returns NaN if the value is
	// negative.  If instead we wanted a precondition that the value was
	// non-negative, we could do the following (this is not a recommendation
	// to change the contract for math.Sqrt).
	sqrt := func(v float64) float64 {
		assert.Assert(v >= 0, "square root undefined for v<0")

		return math.Sqrt(v)
	}

	fmt.Printf("Sqrt of 1 is %0.1f\n", sqrt(1.0))

	// Output:
	// Sqrt of 1 is 1.0
}

func checkAssert(predicate bool, msg string) (ret error) {
	defer func() {
		if r := recover(); r != nil {
			ret = r.(error)
		}
	}()

	assert.Assert(predicate, msg)

	return nil
}

func TestAssert(t *testing.T) {
	cases := []struct {
		predicate bool
		msg       string
		expected  interface{}
	}{
		{true, "trivially true precondition", nil},
		{false, "trivially false precondition", assert.Error("trivially false precondition")},
		{false, "trivially false postcondition", assert.Error("trivially false postcondition")},
	}

	for i, v := range cases {
		ret := checkAssert(v.predicate, v.msg)
		if !reflect.DeepEqual(ret, v.expected) {
			t.Errorf("Case %d:  want %v, got %v", i, v.expected, ret)
		}
	}
}

func BenchmarkAssert(b *testing.B) {
	for i := 0; i < b.N; i++ {
		assert.Assert(true, "a very informative error message")
	}
}
