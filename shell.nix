{ pkgs ? import <nixpkgs> {}, enableLint ? false }:

pkgs.mkShell {
  # No dependencies beyond stdlib.
  buildInputs = if enableLint then
    [ pkgs.go pkgs.golangci-lint ]
  else
    [ pkgs.go ];

  # Make sure we don't pick up the users' GOPATH.
  # Advertise the current version of go when shell starts.
  shellHook = "unset GOPATH; go version;";
}